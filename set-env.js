const { writeFile } = require('fs');

const targetPath = './src/environment.js';

// `environment.ts` file structure
const envConfigFile = `export default {
    backendUrl: '${process.env.BACKEND_URL}',
    repackerUrl: '${process.env.REPACKER_URL}'
};`;

console.log('The file environment.js will be written with the following content: \n');
console.log(envConfigFile);

writeFile(targetPath, envConfigFile, function (err) {
    if (err) {
        throw console.error(err);
    } else {
        console.log(`environment.js file generated correctly at ${targetPath} \n`);
    }
});
