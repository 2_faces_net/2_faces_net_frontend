FROM node:14.18.2-buster-slim as builder

# Move into backend folder
WORKDIR /app

# Copy dependencies json
COPY package.json .
COPY package-lock.json .

# Install node dependencies
RUN npm i && npm audit fix --force

# Copy source files
COPY ./ .

# ENV
ARG BACKEND_URL
ARG REPACKER_URL
ENV BACKEND_URL=$BACKEND_URL
ENV REPACKER_URL=$REPACKER_URL

# Update env vars
RUN node set-env.js

# Start build
RUN npm run build --prod

FROM nginx:1.21.5 as production

COPY --from=builder /app/dist /app
COPY server.conf /etc/nginx/nginx.conf

EXPOSE 80