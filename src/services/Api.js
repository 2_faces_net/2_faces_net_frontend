import axios from 'axios'
import environment from "../environment";

export default {
    backend() {
        return axios.create({
            baseURL: environment.backendUrl
        })
    },
    repack() {
        return axios.create({
            baseURL: environment.repackerUrl
        })
    }
}
